package com.example.midterm1

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private lateinit var adapter:RecyclerViewAdapter
    lateinit var model : InfoModel
    var filteredModel : MutableList<InfoModel.Data> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        getInfo()
    }

    private fun getInfo() {
        DataLoader.getRequest("employees", object: CustomCallback{
            override fun onSuccess(result: String) {

                model = Gson().fromJson(result, InfoModel::class.java)
                filteredModel = model.data
                init()
            }
        })
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(filteredModel)
        recyclerView.adapter = adapter

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        val searchItem = menu?.findItem(R.id.menu_search)

        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView

            searchView.queryHint = "Search by name"

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                        if (newText!!.isNotEmpty()) {
                            val search = newText.toLowerCase()

                            filteredModel.retainAll { person -> person.employeeName.toLowerCase().contains(search) }
                            adapter.notifyDataSetChanged()


                        }else{
                            filteredModel.clear()
                            filteredModel = model.data
                            adapter.notifyDataSetChanged()
                        }
                    return true
                }
            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if  (item.itemId == R.id.menu_profile) {
            startActivity(Intent(this, Profile :: class.java))
        }

        return super.onOptionsItemSelected(item)
    }

}