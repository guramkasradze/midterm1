package com.example.midterm1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*

class RecyclerViewAdapter(private val posts: MutableList<InfoModel.Data>):RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_posts_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:InfoModel.Data
        fun onBind() {
            var activity = HomeActivity()

            model = posts[adapterPosition]
            Glide.with(itemView).load(model.profileImage).into(itemView.imageView)
            itemView.textViewID.text = model.id
            itemView.nameTextView.text = model.employeeName
        }
    }
}













