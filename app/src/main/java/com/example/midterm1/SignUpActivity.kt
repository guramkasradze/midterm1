package com.example.midterm1

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.userProfileChangeRequest
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        init()
    }
    private  fun init() {
        auth = FirebaseAuth.getInstance()

        create_account.setOnClickListener {
            val activity = MainActivity()
            if (activity.validateMail(username) && activity.validatePass(password) && validateSecondPass(password, passwordVerify)) {
                createUser(username.text.toString(), password.text.toString())
            }
        }
        back_sign_up.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun validateSecondPass(passIDo : EditText, passIDt : EditText) : Boolean{
        if (passIDo.text.toString() != passIDt.text.toString()) {
            passwordVerify.error = "Please Enter Same Password"
            passwordVerify.requestFocus()

            return false
        }

        return true
    }

    private fun createUser(mail : String, pass : String) {
        auth.createUserWithEmailAndPassword(mail, pass)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, MainActivity :: class.java))
                    finish()
                } else {
                    Toast.makeText(baseContext, "Sign Up Failed", Toast.LENGTH_SHORT).show()

                }
            }

    }




}