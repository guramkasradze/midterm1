package com.example.midterm1

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.profile_layout.*

class Profile : AppCompatActivity() {

    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private var name :String? = ""
    private var email :String? = ""
    private var uid :String? = ""
    private var photoUrl : Uri? = Uri.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_layout)
        init()
    }

    private fun init() {
        getCurrentUserInfo()

        if (photoUrl != null) { Glide.with(this).load(photoUrl).into(profileImageView) }
        else { Glide.with(this).load("https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png").into(profileImageView); }

        profileIdTextView.text = "#id" + uid
        profileNameTextView.setText(name)
        profileEmailTextView.text = email

        profileBackButton.setOnClickListener {
            startActivity(Intent(this, HomeActivity :: class.java))
        }
        sign_out.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, MainActivity :: class.java))
        }
        profileEditButton.setOnClickListener {
            startActivity(Intent(this, EditProfile :: class.java))
        }
    }

    private fun getCurrentUserInfo() {
        if (user != null) {
            name = user.displayName
            email = user.email
            uid = user.uid
            photoUrl =user.photoUrl
        }

        Log.d("my", "$name $email $uid $photoUrl")
    }
}