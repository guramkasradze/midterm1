package com.example.midterm1

import com.google.gson.annotations.SerializedName

class InfoModel {
    var status = ""
    var data : MutableList<Data> = ArrayList()

    class Data {
        var id = ""
        @SerializedName("employee_name")
        var employeeName = ""
        @SerializedName("employee_salary")
        var employeeSalary = ""
        @SerializedName("employee_age")
        var employeeAge = ""
        @SerializedName("profile_image ")
        var profileImage = "https://www.pngitem.com/pimgs/m/146-1468479_my-profile-icon-blank-profile-picture-circle-hd.png"
    }
}